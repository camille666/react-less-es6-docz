---
name: prettier
menu: 工程文档
---

# prettier

prettier，是一个自以为是 Opinionated 的代码格式化工具，用来批量处理旧代码的统一。
涉及引号，分号，换行，缩进。
prettier 支持我们大前端目前大部分语言处理，包括 JavaScript，Flow，TypeScript，CSS，SCSS，Less，JSX，Vue，GraphQL，JSON，Markdown，这代表着，你几乎可以用一个工具都能搞定所有的代码格式化问题。

[配置文档](https://prettier.io/docs/en/configuration.html)

[演示](https://prettier.io/playground/)

[集成 hook](https://prettier.io/docs/en/precommit.html)

[集成 lint](https://prettier.io/docs/en/integrating-with-linters.html)
