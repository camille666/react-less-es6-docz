---
name: stylelint
menu: 工程文档
---

# stylelint

## 一、参考文档

-   http://stylelint.cn/

-   https://stylelint.io/

-   https://stylelint.io/user-guide/example-config/

-   https://github.com/prettier/stylelint-prettier

-   https://github.com/stylelint/stylelint/blob/master/docs/user-guide/rules.md

stylelint-config-prettier 解决 stylelint 和 prettier 的冲突。

## 二、配置选型

https://www.npmtrends.com/stylelint-config-recommended-vs-stylelint-config-standard

## 三、样式选型

https://www.npmtrends.com/less-vs-stylus-vs-sass

## 四、报错信息

1、团队踩坑

```
Unexpected unknown pseudo-class selector ":global" ：stylint不认global，需要配置selector-pseudo-class-no-unknown。

```
