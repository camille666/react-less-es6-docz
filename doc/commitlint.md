---
name: commitlint
menu: 工程文档
---

# commitlint

这里主要介绍提交信息用到的 cz 工具集。

## 一、生成器

commitizen，cz`生成提交说明`，格式化 git commit message。

```
# 全局安装cz

npm install -g commitizen
yarn global add commitizen

# 提交代码，用git cz代替git commit

git cz

```

## 二、适配器

配置`提交说明`。

1、cz-conventional-changelog

要使用 commitizen 生成符合 AngularJS 规范的提交说明，需要用 cz-conventional-changelog 适配器。

2、cz-customizable

如果想定制项目提交说明，需要用 cz-customizable 适配器。cz-customizable 会去寻找 .cz-config.js 文件。[cz-config](https://github.com/leonardoanalista/cz-customizable/blob/master/cz-config-EXAMPLE.js)

## 三、校验器

`校验提交说明是否符合规范`，可选择 commitlint 或者 validate-commit-msg。

1、[@commitlint/cli](https://commitlint.js.org/#/)

-   安装@commitlint/config-conventional，可以校验提交说明是否符合 Angular 风格。

-   如果使用 cz-customizable 适配器做了破坏 Angular 风格的提交说明配置（比如汉化），那么不能使用@commitlint/config-conventional 进行提交说明校验，可以使用 commitlint-config-cz 对定制化提交说明进行校验，需要用 commitlint-config-cz 校验是否符合规范。

-   概览

    |             | 符合 AngularJS 规范             | 定制化               |
    | ----------- | ------------------------------- | -------------------- |
    | commit 配置 | cz-conventional-changelog       | cz-customizable      |
    | lint 配置   | @commitlint/config-conventional | commitlint-config-cz |

2、validate-commit-msg

[两者 PK](https://www.npmtrends.com/validate-commit-msg-vs-@commitlint/cli)
