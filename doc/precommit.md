---
name: husky
menu: 工程文档
---

# husky

## 一、使用背景

我们有将 lint 命令添加进 npm scripts 中，但是很多人在提交代码时都会忘记或者没有习惯去执行检查，结果就是导致不符合规范的代码被上传到远端代码仓库。

## 二、问题分析

我们可以做 pre-commit 进行`代码强制检测`，也就是在 git commit 之前进行一次代码检测，不符合规范的代码，不让 commit。

## 三、解决方案

### 1、工具选择

https://www.npmtrends.com/git-hooks-vs-husky-vs-pre-commit

### 2、对比分析

-   husky（jquery 与 next.js 都在用）和 pre-commit(antd 在用)，都是 git 钩子。

-   husky 是一个为 git 客户端增加 hook 的工具，能够防止不规范代码被 commit、push、merge，它会在我们项目根目录下面的.git/hooks 文件夹下面创建 pre-commit、pre-push 等 hooks，这些 hooks 可以让我们直接在 package.json 的 script 里运行我们想要在某个 hook 阶段执行的命令。

-   pre-commit 能够防止不规范代码被 commit，但是没有 husky 这么全面，你可以还得安装 pre-push 等插件来防止对应的 git 操作。

### 3、结论

使用 husky 作为 git 钩子。
