---
name: 前端测试
menu: 工程文档
---

# 前端测试

TDD：测试驱动开发，站在程序员的角度，写测试代码。在编写某个功能的代码之前先编写测试代码，然后只编写使测试通过的功能代码，通过测试来推动整个开发的进行。在快速开发并测试功能模块的过程中则更加高效，以快速完成开发为目的。比如 jest。

BDD：行为驱动测试，站在用户的角度，写测试代码。可以让项目成员（甚至是不懂编程的）使用自然描述语言来描述系统功能和业务逻辑，从而根据这些描述步骤进行系统自动化的测试。更偏向于系统功能和业务逻辑的自动化测试设计。

## 一、单元测试（模块测试）

### 1、工具选择

https://www.npmtrends.com/jasmine-vs-jest-vs-karma-vs-mocha-vs-chai

jest 和 mocha 比较流行。jest 的特点是零配置、即时反馈，它所有测试用例默认是并行执行的，速度快，自带断言库和 mock 功能。

可以用 react-dom/test-utils，@testing-library/react，react-testing-library，enzyme。

https://www.npmtrends.com/react-testing-library-vs-enzyme-vs-@testing-library/react

### 2、参考链接

https://jestjs.io/

https://jestjs.io/docs/en/configuration

https://basarat.gitbooks.io/typescript/docs/testing/jest.html

https://github.com/airbnb/enzyme

https://airbnb.io/enzyme/

https://github.com/facebook/jest/tree/master/examples

https://github.com/kulshekhar/ts-jest

## 二、集成测试（组装测试或联合测试）

### 1、工具选择

https://www.npmtrends.com/puppeteer-vs-casperjs-vs-phantomjs

选择 puppeteer。puppeteer 不仅仅可以做自动化集成测试，它本身是个 node 库，自带 chromuium 浏览器（所以 npm 安装它比较慢），他能生成页面屏幕截图或 pdf，自动提交表单，做 UI 测试、模拟键盘输入、鼠标操作等，创建一个最新的自动化测试环境，用最新的 JavaScript 和浏览器功能，直接在最新的 chrome 中做测试，捕获你网站的时间线跟踪，以帮助诊断性能问题。

## 三、功能测试（端到端测试，浏览器测试）

## 四、一些问题

1、测试覆盖率

2、为什么要写在**test**里面
https://jestjs.io/docs/en/configuration#testregex-string-array-string

3、为什么测试文件要用\*.test.ts
https://jestjs.io/docs/en/configuration#testregex-string-array-string

4、it 和 test 的使用
it 是 test 的别名。
https://jestjs.io/docs/en/api.html#testname-fn-timeout
