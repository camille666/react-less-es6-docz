---
name: eslint
menu: 工程文档
---

# eslint

extends 决定了 ESLint 使用何种配置插件进行代码检查，该脚手架启用 ESLint 推荐规则和 React 推荐规则。

eslint-config-standard 是检测的标准（也可以用 airbnb 或者 google），ESLint Config for JavaScript Standard Style。规定检测哪些属性和参数。写在 extends 里面。

eslint-plugin-standard 按标准去检测语法，ESlint Rules for the Standard Linter。规定检测属性和参数的值应该是怎样的。写在 plugin 里面。

eslint-plugin-prettier 插件会调用 prettier 对你的代码风格进行检查，其原理是先使用 prettier 对你的代码进行格式化，然后与格式化之前的代码进行对比，如果过出现了不一致，这个地方就会被 prettier 进行标记。

eslint-config-prettier，能够关闭一些不必要的或者是与 prettier 冲突的 lint 选项，可以解决 eslint 和 prettier 的冲突。

eslint-loader 在编译前执行检查，如果有错误就不让执行。

eslint-loader，eslint，eslint-config-standard，eslint-plugin-standard 之间有什么联系？
