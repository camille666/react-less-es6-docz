import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import Button from '../components/button/button'

class BtnDemo extends React.Component {
    render() {
        return <Button>自定义按钮</Button>
    }
}
export default class HomePage extends React.Component {
    render() {
        return (
            <div>
                <NavLink to="/initpage">初始化</NavLink>
                <Switch>
                    <Route exact={true} path="/initpage" component={BtnDemo} />
                </Switch>
            </div>
        )
    }
}
