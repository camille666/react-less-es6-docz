# 这是一个组件文档工程

## 运行

1、安装依赖

````
yarn install
````

2、启动服务端

````
npm run server
````

3、启动客户端

````
npm run dev
````


4、预览文档系统

````
yarn docz:dev
````

5、构建文档系统

````
docz:build
````

生成的dist文件夹，会出现在.docz的目录下。